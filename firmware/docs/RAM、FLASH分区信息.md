> 以下分区信息仅针对STM32G0B0RET6，更多信息另见文档

## RAM分区

| Name                      | Start Address | Size     |
| ------------------------- | ------------- | -------- |
| IRAM1 (Default) (On Chip) | 0x2000 0000   | 0x2 4000 |

> RAM Total:
>
> 144.0 KB (147456 Bytes)

## FLASH分区

| Name                                | Start Address | Size     |
| ----------------------------------- | ------------- | -------- |
| IROM1 (Default) (On Chip) (Startup) | 0x0800 0000   | 0x4 0000 |
| IROM2 (On Chip)                     | 0x0804 0000   | 0x4 0000 |

> ROM Total:
>
> 256.0 KB (262144 Bytes)