## README

本文件夹存放一些阿里云服务相关功能实现的代码。

- aliyun_config 存放阿里云连接信息配置
- aliyun_dynreg 阿里云动态注册功能实现
- aliyun_message_handle 向阿里云发送数据的处理
- aliyun_ntp 阿里云NTP服务实现
- aliyun_ota 阿里云OTA功能实现
- aliyun_protocol 该文件是阿里云功能实现的入口，所有功能统一由此处进行初始化与调用
- aliyun_shadow 阿里云设备影子功能实现