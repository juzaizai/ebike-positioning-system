/*
 * @Author: 橘崽崽啊 2505940811@qq.com
 * @Date: 2023-09-21 12:21:15
 * @LastEditors: 橘崽崽啊 2505940811@qq.com
 * @LastEditTime: 2023-09-22 17:04:53
 * @FilePath: \firmware\user\main\app_main.h
 * @Description: APP主函数头文件
 * 
 * Copyright (c) 2023 by 橘崽崽啊 2505940811@qq.com, All Rights Reserved. 
 */
#ifndef __APP_MAIN_H
#define __APP_MAIN_H

/// @brief 关闭ec800m并重启mcu
void ec800m_poweroff_and_mcu_restart(void);

#endif // !__APP_MAIN_H
