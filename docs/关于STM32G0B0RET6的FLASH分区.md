# 关于STM32G0B0RET6的分区

> 注意：
>
> 本固件需要使用Keil AC6编译器进行编译，使用GCC编译可能会导致固件体积过大无法烧录进STM32G0B0RET6

## 硬件

- 设备型号：STM32G0B0RET6

- FLASH大小：512KB

- RAM大小：144KB
- 页大小：2KB
- 最小擦除单元：页擦除

## 分区

#### 方案一

| 分区名称   | 起始地址    | 结束地址    | 分区大小    |       |
| ---------- | ----------- | ----------- | ----------- | ----- |
| BootLoader | 0x0800 0000 | 0x0800 3000 | 0x0000 3000 | 12KB  |
| APP1       | 0x0800 3000 | 0x0804 1800 | 0x0003 E800 | 250KB |
| APP2       | 0x0804 1800 | 0x0808 0000 | 0x0003 E800 | 250KB |

#### 方案二

| 分区名称   | 起始地址    | 结束地址    | 分区大小    |       |
| ---------- | ----------- | ----------- | ----------- | ----- |
| BootLoader | 0x0800 0000 | 0x0800 3000 | 0x0000 3000 | 12KB  |
| APP1       | 0x0800 3000 | 0x0804 0000 | 0x0003 D000 | 244KB |
| APP2       | 0x0804 0000 | 0x0808 0000 | 0x0004 0000 | 256KB |

#### 方案三

| 分区名称 | 起始地址    | 结束地址    | 分区大小    |       |
| -------- | ----------- | ----------- | ----------- | ----- |
| APP1     | 0x0800 0000 | 0x0804 0000 | 0x0004 0000 | 256KB |
| APP2     | 0x0804 0000 | 0x0808 0000 | 0x0004 0000 | 256KB |

> 目前采用方案三
